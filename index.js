/*top nav code*/
function myFunction() {
  var x = document.getElementById("myTopnav");                  /*returns an element with a specified value*/
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
/*side nav code*/
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";    /*returns an element with a specified value*/
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";

}
/*slide show code*/
let slideIndex = 1;
showSlides(slideIndex);             /*display the  image*/

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}
function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
